getHelpFor <- function(fname){
  if(fname == "prodscal"){
    cat("x\t vector\n")
    cat("y\t matrix or data.frame\n")
    cat("--\n")
    cat("x is multiplied with rows of y to obtain scalar products\n")
  }else if(fname == "norm.L2"){
    cat("x\n vector or matrix\n")
    cat("--\n")
    cat("calculates norms for the rows of x (or for x if x is a vector) \n")
  }
}

#Euclidean norm
norm.L2 <- function(x){
  if(is.vector(x)){
    x <- rbind(x)
  }
  sqrt(apply(x^2, 1, sum))
  #sqrt(sum(x^2))
}

#x two-component vector
#X two-column matrix
distTo <- function(x, X){
  X <- as.matrix(X)
  apply(t(X)-x, 2, norm.L2)
}

#Mahalonobis distance
norm.M <- function(x, M){
  if(is.matrix(x)) x <- t(x)
  sqrt(apply(x * M %*% x, 2, sum))
}

prodscal <- function(x,y){
  apply((x*t(as.matrix(y))), 2, sum)
}