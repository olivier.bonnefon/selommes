cropaalo <- function(landscape,prop,aggr){
    




coordpaysage <- coordinates(landscape)
numpaysage <- sapply(slot(landscape, "polygons"), function(x) slot(x, "ID"))
numpaysage <- as.integer(numpaysage)
aireparcelles <- sapply(slot(landscape, "polygons"), function(x) slot(x, "area"))
    


#champ gaussien
coor <- data.frame(x=coordpaysage[,1],y=coordpaysage[,1])
d <- as.matrix(dist(coor))

if (aggr>0){
    cov_mat<-Exponential(d,range=aggr,phi=10)  # function from 'fields' package
    s<-mvrnorm(1,mu=rep(0,length(landscape)),Sigma=cov_mat) # function from 'MASS' package
} else{
    s <- rnorm(length(landscape),mean=0,sd=1)
}

data.agr<-as.data.frame(s)
toto <- data.frame(numpaysage,"s"=data.agr$s)
o <- order(toto$s)
toto2 <- data.frame(cbind("numpaysage"=toto$numpaysage[o], "s.ord"=toto$s[o]))  #champ gaussien croissant
   
#aires des parcelles
aire.sort <- aireparcelles[o]
cumsum.aire <- cumsum(aire.sort)
toto3 <- cbind(toto2,aire.sort,cumsum.aire)
    
# seuil pour allocation de R
prop.airetot <- prop*sum(aireparcelles)
toto4 <- cbind(toto3,"dif"=abs(cumsum.aire-rep(prop.airetot,length(cumsum.aire))))
indice.seuil <- seq(1,dim(toto4)[1])[which.min(toto4$dif)]
    
# repartition de R et S
repvar <- rep(0,length(landscape))
repvar[toto4$numpaysage[1:indice.seuil]] <- 1
repvar <- data.frame(repvar)
    
#représentation
paysagealloc <- SpatialPolygonsDataFrame(landscape, repvar, match.ID = T)

    return(paysagealloc)
}


    


