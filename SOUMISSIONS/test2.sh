#!/bin/bash
#$ -S /bin/bash
#$ -N EmiSt
#$ -V
#$ -cwd

echo "lot $nlot par $JOB_ID "
echo "taillelot=$taillelot"

OUTPUTDIR=../SIMU/SELOMMES_tmp$nlot"/"
mkdir $OUTPUTDIR
cd $OUTPUTDIR
GEOREP=../../GEODATA/SELOMMES/
cp $GEOREP/Points.txt .
cp $GEOREP/Edges.txt .
cp $GEOREP/Wires.txt .

for ((i=1;i<$((taillelot+1));i++));do
       numsimu=$((i+taillelot*(nlot-1))) 
       if [ ! -e ../../RESULTS/simudone$numsimu ]; then
	   echo "doing $numsimu"
	   /usr/bin/R3 --no-save --args $numsimu <'../script_cluster.R'
	   # role de script_cluster.R
	   # modifier la geometrie en tenant compte de  et de i
	   #     -> modifier $OUTPUTDIR/edges.txt
	   #     -> modifier $OUTPUTDIR/cultures.txt
	   #     -> modifier $OUTPUTDIR/events.txt en tenant compte de KK et de i
	   #     -> modifier $OUTPUTDIR/summaryParams.edp
	   rm control*.txt
	   /opt/SELOMMES/buildSimulateurSelommes . >/dev/null

	   rm FFend.txt
	   /opt/freefem++.lh/src/nw/FreeFem++-nw cas1.edp >/dev/null
	   if [ ! -e FFend.txt]; then
	       echo "FF failed to silumate $numsimu"
	   else
	       # sauvegarder des sortie de simu
	       cp mass.txt ../../RESULTS/mass$numsimu.txt    
	       cp Edges.txt ../../RESULTS/Edges$numsimu.txt
	       cp culture.txt ../../RESULTS/culture$numsimu.txt
	       cp event.txt ../../RESULTS/event$numsimu.txt
	       cp summaryParams.edp ../../RESULTS/summaryParams$numsimu.edp
	       
	       more control*.txt >../../RESULTS/controls$numsimu.txt
	       
	       
	       rm ../../RESULTS/summaryEvent$numsimu.txt
	       touch ../../RESULTS/summaryEvent$numsimu.txt
	       rm ../../RESULTS/summaryControl$numsimu.txt
	       touch ../../RESULTS/summaryControl$numsimu.txt
	       for ((np=0;np<188;np++));do
		   grep traitement control$np.txt | grep debut | wc -l >> ../../RESULTS/summaryControl$numsimu.txt
		   grep source control$np.txt | grep noise | wc -l >> ../../RESULTS/summaryEvent$numsimu.txt
	       done
	       touch ../../RESULTS/simudone$numsimu
	   fi
       else
	   echo "skip it because file ../../RESULTS/culture$numsimu.txt exists"
       fi
done

