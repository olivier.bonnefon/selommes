setwd("~/research/implementations/landscape/script-cluster/")

#full factorial design is generated in the following.
#design is stored as a table in configs.txt.

#list of parameters:
#(if a parameter varies over different values, these values are given as a vector in the following)

#number of simulations for each configuration:
nsimul=20

#range1, range2 of Gaussian fields used to simulate crops and hedges:
maxdist=5.546123
range1=maxdist/c(1,15,100)
#range2=range1
#correlation parameters (rho1=1 is fixed, such that rho2 indicates correlation between crop and hedge field):
rho1=1
rho2=.5
#proportion of hedges (with respect to overall length of edges) and crops (with respect to overall surface) in landscape:
prop.h=c(.2,.5,.8)
prop.c=c(.2,.5,.8)
#average number of inoculations over the observation period:
intens.full=25

#parameters of population dynamics model:
DCultureProie=c(0.2,3,8) #valeur min: 0.0625 #valeur max: 12
DCulturePredator=c(0.2,3,8) #valeur min: 0.0625 #valeur max: 12   
DEdgePredator=12 
scale1D2D=c(.1,.5,1) #valeur min: 0.1 #valeur max: 1  

#list containing all parameter values:
#parlist=list(range1=range1,range2=range2,rho1=rho1,rho2=rho2,prop.h=prop.h,prop.c=prop.c,intens.full=intens.full,DCultureProie=DCultureProie,DCulturePredator=DCulturePredator,DEdgePredator=DEdgePredator,scale1D2D=scale1D2D)
parlist=list(range1=range1,rho1=rho1,rho2=rho2,prop.h=prop.h,prop.c=prop.c,intens.full=intens.full,DCultureProie=DCultureProie,DCulturePredator=DCulturePredator,DEdgePredator=DEdgePredator,scale1D2D=scale1D2D)

#create a full factorial design and store into data.frame, with nsimul repetitions for each configuration:
configs.df=expand.grid(parlist)
configs.df$id.config=1:nrow(configs.df)
nconfigs=nrow(configs.df)
dim(configs.df)
head(configs.df)
configs.df=configs.df[rep(1:nrow(configs.df),each=nsimul),]
configs.df$id.simu=1:nrow(configs.df)
row.names(configs.df)=1:nrow(configs.df)
configs.df$range2=configs.df$range1
dim(configs.df)

write.table(configs.df,file="configs.txt",col.names=TRUE,row.names=FALSE)


##
##

#---------------------------------------------------------------------------    
# 1) création du plan de simulations pour la méthode de Sobol
#---------------------------------------------------------------------------        

library(lhs)
library(sensitivity)

# npar = nbre de facteurs
# ndecoup = nbre de découpe de la gamme d'un facteur pour un LHS = nbre de points dans l'espace

ndecoup <- 500
npar <- 6

graine <- 1900.12
set.seed(graine)

X1 <- lhs::randomLHS(ndecoup,npar)
X2 <- lhs::randomLHS(ndecoup,npar)

# attention à l'ordre ; order=0 => Itot
plan1 <- sobolEff(model=NULL,  X1=X1, X2=X2, order=1)
plan0 <- sobolEff(model=NULL, X1=X1, X2=X2, order=0)

PLAN <- rbind(plan1$X, plan0$X[-(1:ndecoup),])

nsimul <- 15

#range1, range2 of Gaussian fields used to simulate crops and hedges: range1=range2
maxdist <- 5.546123
range1 <- c(maxdist/100,maxdist/1)

#correlation parameters (rho1=1 is fixed, such that rho2 indicates correlation between crop and hedge field):
rho1 <- 1
rho2 <- .5

#proportion of hedges (with respect to overall length of edges) and crops (with respect to overall surface) in landscape:
prop.h <- c(0,1)
prop.c <- c(0,1)

#average number of inoculations over the observation period:
intens.full <- 25

#parameters of population dynamics model:
DCultureProie <- c(0.0625,12)
DCulturePredator <- c(0.0625,12)
DEdgePredator <- 12 
scale1D2D <- c(0.1,1)

configs.df <- data.frame(id.config=1:nrow(PLAN))
configs.df$range1 <- PLAN[,1]*(max(range1)-min(range1))+min(range1)
configs.df$prop.h <- PLAN[,2]*(max(prop.h)-min(prop.h))+min(prop.h)
configs.df$prop.c <- PLAN[,3]*(max(prop.c)-min(prop.c))+min(prop.c)
configs.df$DCultureProie <- PLAN[,4]*(max(DCultureProie)-min(DCultureProie))+min(DCultureProie)
configs.df$DCulturePredator <- PLAN[,5]*(max(DCulturePredator)-min(DCulturePredator))+min(DCulturePredator)
configs.df$scale1D2D <- PLAN[,6]*(max(scale1D2D)-min(scale1D2D))+min(scale1D2D)
configs.df$rho1 <- rep(rho1,nrow(PLAN))
configs.df$rho2 <- rep(rho2,nrow(PLAN))
configs.df$intens.full <- rep(intens.full,nrow(PLAN))
configs.df$DEdgePredator <- rep(DEdgePredator,nrow(PLAN))
nconfigs <- nrow(configs.df)
dim(configs.df)
head(configs.df)
configs.df <- configs.df[rep(1:nrow(configs.df),each=nsimul),]
configs.df$id.simu <- 1:nrow(configs.df)
row.names(configs.df) <- 1:nrow(configs.df)
configs.df$range2 <- configs.df$range1
dim(configs.df)
head(configs.df)

write.table(configs.df,file="configs.txt",col.names=TRUE,row.names=FALSE)
