# Les étatapes précédent l'appel de buildSimulateurSelommes sont faites par les scripts R lors des soumissions.
OUTPUTDIR=/mnt/stockage/partage/paysage/SELOMMES/SIMU/RUN1/
mkdir $OUTPUTDIR
GEOREP=/mnt/stockage/partage/paysage/SELOMMES/GEODATA/SELOMMES/
cp $GEOREP/Points.txt $OUTPUTDIR
cp $GEOREP/Edges.txt $OUTPUTDIR
cp $GEOREP/Wires.txt $OUTPUTDIR
cp $GEOREP/culture.txt $OUTPUTDIR

./buildSimulateurSelommes $OUTPUTDIR

