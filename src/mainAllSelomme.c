#include "gen2speciesNL.c"

#include "tessellation.h"



int main(int argc, char *argv[]){
  int ii;
  if (argc != 2){
    printf("usage: buildSimulateurSelommes /mnt/../SELOMMES\n");
    return 1;
  }else{
//    strcpy(OUTPUTDIR,argv[1]);
    sprintf(OUTPUTDIR,"%s/",argv[1]);
    printf("doing update in directory %s\n",OUTPUTDIR);
  }
  sprintf(POSTPRODIR,"/mnt/stockage/partage/paysage/SELOMMES/src/postpro/");
  sprintf(GEODIR,"%s",OUTPUTDIR);
  initStructure();
  buildDirectories();

  //exit(0);
  //randomizePoint(1.5);
  initMatStruct();
//  printViewVtk();
  printVtkViewWires();
  printSaveMeshes();
  
  printParams();
   
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defOutputGen.edp");
  FILE *pOut = fopen(FILENAME, "w");
  buildDefOutput(pOut);
  fclose(pOut);

//  return 1;
  if (BUILD_MESH){
    sprintf(FILENAME,"%s%s",OUTPUTDIR,"defGeomGen.edp");
    FILE *pGeom = fopen(FILENAME, "w");
//  printPoints(pGeom);
    printEdges(pGeom);
    printMeshes(pGeom);
    fclose(pGeom);
  }

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defDofsGen.edp");
  FILE *pDofs = fopen(FILENAME, "w");
  printDofs(pDofs);
  fclose(pDofs);
  
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defDofs2DGen.edp");
  pDofs = fopen(FILENAME, "w");
  printDofs2D(pDofs);
  fclose(pDofs);
  if (NDynEdgeType){
    sprintf(FILENAME,"%s%s",OUTPUTDIR,"defDofs1DGen.edp");
    pDofs = fopen(FILENAME, "w");
    printDofs1D(pDofs);
    fclose(pDofs);
  }

  
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defVarfGen.edp");
  FILE *pVarfs = fopen(FILENAME, "w");
  printVarfs(pVarfs);
  fclose(pVarfs);

 

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defMatGen.edp");
  FILE *pMats = fopen(FILENAME, "w");
  printMat(pMats);
  fclose(pMats);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"buildMatGen.edp");
  FILE *pMatsBuild = fopen(FILENAME, "w");
  buildMat(pMatsBuild);
  fclose(pMatsBuild);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"timeSteppingGen.edp");
  FILE *pTimeStep = fopen(FILENAME, "w");
  printTS(pTimeStep);
  fclose(pTimeStep);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"timeSteppingOneStepGen.edp");
  FILE *pTimeStepOneStep = fopen(FILENAME, "w");
  printTSOneStep(pTimeStepOneStep);
  fclose(pTimeStepOneStep);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"solToDof.edp");
  FILE *pTimeStepSolToDof = fopen(FILENAME, "w");
  printsolToDof(pTimeStepSolToDof);
  fclose(pTimeStepSolToDof);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defPluginVaraibleGen.edp");
  FILE *pPlugV = fopen(FILENAME, "w");
  printPluginVariable(pPlugV);
  fclose(pPlugV);

  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defPluginGen.edp");
  FILE *pPlug = fopen(FILENAME, "w");
  printPlugin(pPlug);
  fclose(pPlug);
printf("rep0\n");
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defICGen.edp");
  //if(access( FILENAME, F_OK ) == -1){
    FILE *pIC = fopen(FILENAME, "w");
    printf("call IC\n");
    printInitialCond(pIC,1);
    fclose(pIC);
    //}
    printf("rep1\n");
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"massInfo.txt");
  FILE *pMassInfo = fopen(FILENAME,"w");
  printMassInfo(pMassInfo);
  fclose(pMassInfo);
printf("rep2\n");
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"defXXYYGen.edp");
  FILE *pXY = fopen(FILENAME, "w");
  printXXYY(pXY);
  fclose(pXY);
  
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"vtktopngGen.edp");
  FILE *pVtkLim = fopen(FILENAME,"w");
  printVTKGENPY(pVtkLim);
  fclose(pVtkLim);
  
  sprintf(FILENAME,"%s%s",OUTPUTDIR,"run.sh");
  FILE *pRunsh = fopen(FILENAME,"w");
  buildRunSGEFile(pRunsh);
  fclose(pRunsh);
  freeEdges();
  resetMatStruct();
  freeStructs();
  printf("repfin\n");
  
  for (ii=0;ii<NFilesCopy;ii++){
    sprintf(FILENAME,"cp %s%s %s",POSTPRODIR,FILES_COPY[ii],OUTPUTDIR);
    printf("doing :%s\n",FILENAME);
    system(FILENAME);
  }
  return 0;
}
